﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;

public class GameManager : SerializedMonoBehaviour
{
    public static GameManager Instance;

    public BoardManager bm;
    public DeckManager dm;
    public WinManager wm;
    public TBManager tbm;

    public GameObject deckBuildingCanvas;

    private GameObject[,] gamePieces;

    private Player whiteTeam;
    private Player blackTeam;
    public Player currentPlayer;
    public Player nonCurrentPlayer;

    public GameObject whiteRook;
    public GameObject whiteKnight;
    public GameObject whiteBishop;
    public GameObject whiteQueen;
    public GameObject whiteKing;
    public GameObject whitePawn;
    public GameObject blackRook;
    public GameObject blackKnight;
    public GameObject blackBishop;
    public GameObject blackQueen;
    public GameObject blackKing;
    public GameObject blackPawn;

    public TextMeshProUGUI CurrentPlayerLabel;
    public string winnerLabel;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        //Initialization();
    }

    public void Initialization()
    {
        gamePieces = new GameObject[8, 8];

        // Testing only, will assign player their username for name Post-Capstone.
        whiteTeam = new Player("White", dm.whiteDeck, true, true);
        blackTeam = new Player("Black", dm.blackDeck, false, false);

        currentPlayer = whiteTeam;
        nonCurrentPlayer = blackTeam;

        CurrentPlayerLabel.text = "It is " + currentPlayer.playerName + "'s turn!";

        //int blackCount = 0;

        //for (int i = 1; i > -1; i--)
        //{
        //    for (int j = 0; j < 8; j++)
        //    {
        //        MeshRenderer[] blackMats = dm.blackDeckCards[blackCount].piecePrefab.GetComponentsInChildren<MeshRenderer>();

        //        foreach (MeshRenderer mr in blackMats)
        //        {
        //            mr.material = bm.originalBlackMaterial;
        //        }

        //        //Debug.Log(dm.blackDeckCards[blackCount].piecePrefab.name + " was added at (" + i + "," + j + ").");
        //        AddPiece(dm.blackDeckCards[blackCount].piecePrefab, blackTeam, i, j);
        //        blackCount++;
        //    }
        //}

        //int whiteCount = 0;

        //for (int i = 6; i < 8; i++)
        //{
        //    for (int j = 0; j < 8; j++)
        //    {
        //        MeshRenderer[] whiteMats = dm.whiteDeckCards[whiteCount].piecePrefab.GetComponentsInChildren<MeshRenderer>();

        //        foreach (MeshRenderer mr in whiteMats)
        //        {
        //            mr.material = bm.originalWhiteMaterial;
        //        }

        //        //Debug.Log(dm.whiteDeckCards[whiteCount].piecePrefab.name + " was added at (" + i + "," + j + ").");
        //        AddPiece(dm.whiteDeckCards[whiteCount].piecePrefab, whiteTeam, i, j);
        //        whiteCount++;
        //    }
        //}

        deckBuildingCanvas.SetActive(false);

       AddPiece(whiteRook, whiteTeam, 0, 0);
        AddPiece(whiteKnight, whiteTeam, 1, 0);
        AddPiece(whiteBishop, whiteTeam, 2, 0);
        AddPiece(whiteQueen, whiteTeam, 3, 0);
        AddPiece(whiteKing, whiteTeam, 4, 0);
        AddPiece(whiteBishop, whiteTeam, 5, 0);
        AddPiece(whiteKnight, whiteTeam, 6, 0);
        AddPiece(whiteRook, whiteTeam, 7, 0);

        for (int i = 0; i < 8; i++)
        {
            AddPiece(whitePawn, whiteTeam, i, 1);
        }

        AddPiece(blackRook, blackTeam, 0, 7);
        AddPiece(blackKnight, blackTeam, 1, 7);
        AddPiece(blackBishop, blackTeam, 2, 7);
        AddPiece(blackQueen, blackTeam, 3, 7);
        AddPiece(blackKing, blackTeam, 4, 7);
        AddPiece(blackBishop, blackTeam, 5, 7);
        AddPiece(blackKnight, blackTeam, 6, 7);
        AddPiece(blackRook, blackTeam, 7, 7);

        for (int i = 0; i < 8; i++)
        {
            AddPiece(blackPawn, blackTeam, i, 6);
        }
    }

    public void AddPiece(GameObject piecePrefab, Player playerTeam, int column, int row)
    {
        GameObject pieceToAdd = bm.AddPiece(piecePrefab, column, row);
        playerTeam.playerPieces.Add(pieceToAdd);
        gamePieces[column, row] = pieceToAdd;
    }

    public void SelectPieceAtGridPoint(Vector2Int gridPoint)
    {
        GameObject pieceToSelect = gamePieces[gridPoint.x, gridPoint.y];

        if (pieceToSelect)
            bm.SelectPiece(pieceToSelect);
    }

    public List<Vector2Int> GetMovementForPiece(GameObject pieceToCheck)
    {
        Piece pieceRef = pieceToCheck.GetComponent<Piece>();
        Vector2Int gridPoint = GetPieceLocation(pieceToCheck);
        List<Vector2Int> movementLocations = pieceRef.MovementOptions(gridPoint);

        // Get rid of locations that don't exist on the board.
        movementLocations.RemoveAll(gp => gp.x > 7 || gp.x < 0 || gp.y > 7 || gp.y < 0);

        // Get rid of locations with friendlies.
        movementLocations.RemoveAll(gp => CheckFriendliesAt(gp));

        return movementLocations;
    }

    public void MovePiece(GameObject pieceToMove, Vector2Int gridpoint)
    {
        //Debug.Log(pieceToMove.name + " at " + gridpoint.ToString());
        Vector2Int staringGridPoint = GetPieceLocation(pieceToMove);
        gamePieces[staringGridPoint.x, staringGridPoint.y] = null;
        gamePieces[gridpoint.x, gridpoint.y] = pieceToMove;
        bm.MovePiece(pieceToMove, gridpoint);
    }

    public void CaptureAt(Vector2Int gridPoint)
    {
        GameObject pieceToCapture = PieceAtGridPoint(gridPoint);

        if (pieceToCapture.GetComponent<Piece>().pieceType == PieceType.King)
        {
            wm.WinGame(winnerLabel);
            //Debug.Log(currentPlayer.playerName + " has defeated " + nonCurrentPlayer.playerName);
            Destroy(bm.GetComponent<TileSelectionHandler>());
            Destroy(bm.GetComponent<MoveSelectionHandler>());
        }

        currentPlayer.playerCapturedPieces.Add(pieceToCapture);
        gamePieces[gridPoint.x, gridPoint.y] = null;
        Destroy(pieceToCapture);
    }

    public void SelectPiece(GameObject pieceToSelect)
    {
        bm.SelectPiece(pieceToSelect);
    }

    public void DeselectPiece(GameObject pieceToDeselect)
    {
        bm.DeselectPiece(pieceToDeselect);
    }

    public bool IsPieceOnCurrentTeam(GameObject pieceToCheck)
    {
        return currentPlayer.playerPieces.Contains(pieceToCheck);
    }

    public Vector2Int GetPieceLocation(GameObject pieceToFind)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (gamePieces[i, j] == pieceToFind)
                {
                    //Debug.Log("Piece Found: " + pieceToFind.name + " at (" + i + "," + j + ")");
                    return new Vector2Int(i, j);
                }
            }
        }

        // Piece wasn't found.
        //Debug.Log("Piece Not Found");
        return new Vector2Int(-1, -1);
    }

    public GameObject PieceAtGridPoint(Vector2Int gridPoint)
    {
        if (gridPoint.x > 7 || gridPoint.x < 0 || gridPoint.y > 7 || gridPoint.y < 0)
            return null;

        return gamePieces[gridPoint.x, gridPoint.y];
    }

    public bool CheckFriendliesAt(Vector2Int gridPoint)
    {
        GameObject friendlyPiece = PieceAtGridPoint(gridPoint);

        if (friendlyPiece == null)
            return false;

        if (nonCurrentPlayer.playerPieces.Contains(friendlyPiece))
            return false;

        return true;
    }

    public void ChangePlayers()
    {
        Player temp = currentPlayer;
        currentPlayer = nonCurrentPlayer;
        nonCurrentPlayer = temp;
        CurrentPlayerLabel.text = "It is " + currentPlayer.playerName + "'s turn!";
    }
}