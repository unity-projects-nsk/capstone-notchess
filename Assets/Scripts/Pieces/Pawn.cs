﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{
    public override List<Vector2Int> MovementOptions(Vector2Int gridPoint)
    {
        List<Vector2Int> locations = new List<Vector2Int>();

        int forwardDirection = GameManager.Instance.currentPlayer.forwardDirection;

        Vector2Int forwardOne = new Vector2Int(gridPoint.x, gridPoint.y + forwardDirection);
        if (GameManager.Instance.PieceAtGridPoint(forwardOne) == false)
        {
            locations.Add(forwardOne);
        }

        Vector2Int forwardRight = new Vector2Int(gridPoint.x + 1, gridPoint.y + forwardDirection);
        if (GameManager.Instance.PieceAtGridPoint(forwardRight))
        {
            locations.Add(forwardRight);
        }

        Vector2Int forwardLeft = new Vector2Int(gridPoint.x - 1, gridPoint.y + forwardDirection);
        if (GameManager.Instance.PieceAtGridPoint(forwardLeft))
        {
            locations.Add(forwardLeft);
        }

        return locations;
    }
}
