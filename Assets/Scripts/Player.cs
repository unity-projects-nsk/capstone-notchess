﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player class that defines the pieces and movement direction of each player.
public class Player
{
    public List<GameObject> playerPieces;
    public List<GameObject> playerCapturedPieces;

    public string playerName;
    public int forwardDirection;
    public bool isWhite;

    // Constructor
    public Player(string playerName, List<GameObject> deck, bool isZMovementPositive, bool isWhite)
    {
        this.playerName = playerName;
        this.isWhite = isWhite;
        playerPieces = deck;
        playerCapturedPieces = new List<GameObject>();
        forwardDirection = isZMovementPositive ? 1 : -1;
    }
}