﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

// Defines a PieceType enum that handles the overall type of a given piece
public enum PieceType { King, Queen, Bishop, Knight, Rook, Pawn }

// An abstract class used to define all pieces
public abstract class Piece : SerializedMonoBehaviour
{
    public Card cardInfo;
    public PieceType pieceType;

    // Replace later
    protected Vector2Int[] RookDirections = {new Vector2Int(0,1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0)};
    protected Vector2Int[] BishopDirections = {new Vector2Int(1,1), new Vector2Int(1, -1), new Vector2Int(-1, -1), new Vector2Int(-1, 1)};

    // Overrideable class that shows movement options for a give piece.
    public abstract List<Vector2Int> MovementOptions(Vector2Int gridPoint);
}

