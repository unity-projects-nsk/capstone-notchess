﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using System;

public class DeckSpaceHandler : SerializedMonoBehaviour
{
    public DeckManager dm;

    public PieceLocation location;
    PieceType type;
    public Card cardInSpace;

    private void Awake()
    {
        SetSpaceType();
    }

    public void SetCardToSpace()
    {
        // Returns out if no card is selected
        if (dm.currentlySelectedCard == null)
            return;

        // Checks to make sure card selected has same type as space selected
        if (IsCardCompatible())
        {
            cardInSpace = dm.currentlySelectedCard;

            // Set Deck Space Button to relevant card image
            GetComponent<Button>().image.sprite = cardInSpace.cardImage;

            if (dm.whiteDeckDisplay.activeSelf)
            {
                dm.whiteDeckCards[Array.IndexOf(Enum.GetValues(location.GetType()), location)] = cardInSpace;
            }

            if (dm.blackDeckDisplay.activeSelf)
            {
                dm.blackDeckCards[Array.IndexOf(Enum.GetValues(location.GetType()), location)] = cardInSpace;
            }

        }
    }

    bool IsCardCompatible()
    {
        if (dm.currentlySelectedCard.pieceType != type)
            return false;
        else
            return true;
    }

    void SetSpaceType()
    {
        switch (location)
        {
            case PieceLocation.King:
                type = PieceType.King;
                break;
            case PieceLocation.Queen:
                type = PieceType.Queen;
                break;
            case PieceLocation.Bishop_Left:
                type = PieceType.Bishop;
                break;
            case PieceLocation.Bishop_Right:
                type = PieceType.Bishop;
                break;
            case PieceLocation.Knight_Left:
                type = PieceType.Knight;
                break;
            case PieceLocation.Knight_Right:
                type = PieceType.Knight;
                break;
            case PieceLocation.Rook_Left:
                type = PieceType.Rook;
                break;
            case PieceLocation.Rook_Right:
                type = PieceType.Rook;
                break;
            case PieceLocation.Pawn_1:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_2:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_3:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_4:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_5:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_6:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_7:
                type = PieceType.Pawn;
                break;
            case PieceLocation.Pawn_8:
                type = PieceType.Pawn;
                break;
            default:
                type = PieceType.Pawn;
                break;
        }
    }

}
