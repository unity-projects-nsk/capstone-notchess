﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class MainMenuManager : SerializedMonoBehaviour
{
    public Dictionary<string, string> accounts;
    string accountsFile = "accounts.save";

    public GameObject MainMenuCanvas;
    public GameObject CreateAccountCanvas;
    public GameObject LogInCanvas;

    public Button playMatch;
    public TextMeshProUGUI logInText;

    public TMP_InputField createAccount_Username;
    public TMP_InputField createAccount_Password;
    public TMP_InputField createAccount_PasswordConfirm;

    public TMP_InputField logInAccount_Username;
    public TMP_InputField logInAccount_Password;

    public TextMeshProUGUI CreateAccountError;
    public TextMeshProUGUI LogInError;

    public TextMeshProUGUI CreateAccountSuccess;
    public TextMeshProUGUI LogInSuccess;

    string expectedPassword;

    void Awake()
    {
        if (Serializer.Load<Dictionary<string, string>>(accountsFile) != null)
            accounts = Serializer.Load<Dictionary<string, string>>(accountsFile);
        else
            accounts = new Dictionary<string, string>();

        createAccount_Password.text = "";
        createAccount_PasswordConfirm.text = "";
        createAccount_Username.text = "";
        logInAccount_Password.text = "";
        logInAccount_Username.text = "";
    
    }

    public void PlayGame()
    {
        Serializer.Save<Dictionary<string, string>>(accountsFile, accounts);
        SceneManager.LoadScene("Game");
    }

    public void ExitGame()
    {
        Debug.Log("Application exit if not in editor.");
        Application.Quit(0);
    }

    public void CreateAccount()
    {
        createAccount_Password.text = "";
        createAccount_PasswordConfirm.text = "";
        createAccount_Username.text = "";
        logInAccount_Password.text = "";
        logInAccount_Username.text = "";

        expectedPassword = "";

        MainMenuCanvas.SetActive(false);
        CreateAccountCanvas.SetActive(true);
        LogInCanvas.SetActive(false);
    }

    public void LogIn()
    {
        createAccount_Password.text = "";
        createAccount_PasswordConfirm.text = "";
        createAccount_Username.text = "";
        logInAccount_Password.text = "";
        logInAccount_Username.text = "";

        expectedPassword = "";

        MainMenuCanvas.SetActive(false);
        CreateAccountCanvas.SetActive(false);
        LogInCanvas.SetActive(true);
    }

    public void ReturnToMain()
    {
        createAccount_Password.text = "";
        createAccount_PasswordConfirm.text = "";
        createAccount_Username.text = "";
        logInAccount_Password.text = "";
        logInAccount_Username.text = "";

        expectedPassword = "";

        MainMenuCanvas.SetActive(true);
        CreateAccountCanvas.SetActive(false);
        LogInCanvas.SetActive(false);
    }

    public void AddAccount()
    {
        if (!createAccount_Username.text.Equals("") && !createAccount_Password.text.Equals("") && !createAccount_PasswordConfirm.text.Equals(""))
        {
            if (createAccount_Password.text.Equals(createAccount_PasswordConfirm.text))
            {
                accounts.Add(createAccount_Username.text, createAccount_Password.text);
                CreateAccountSuccess.text = "Account created! Return to main menu!";
            }
            else
            {
                CreateAccountError.text = "Passwords do not match!";
            }
                
        } else
        {
            CreateAccountError.text = "All fields required!";
        }
    }

    public void OpenAccount()
    {
        if (!logInAccount_Username.text.Equals("") && !logInAccount_Password.text.Equals(""))
        {
            expectedPassword = logInAccount_Password.text;
            if (accounts.TryGetValue(logInAccount_Username.text, out string actualPassword))
            {
                if (actualPassword.Equals(expectedPassword))
                {
                    SetAccountInfo(logInAccount_Username.text);
                    LogInSuccess.text = "Log in was successful! Return to main menu!";
                }
                else
                {
                    LogInError.text = "Password is incorrect!";
                }
            }
            else
            {
                LogInError.text = "Username doesn't exist!";
            }
        }
        else
        {
            LogInError.text = "All fields required!";
        }
    }

    void SetAccountInfo(string logInName)
    {
        logInText.text = "Welcome, " + logInName + "!";
        playMatch.interactable = true;
    }
}