﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class WinManager : MonoBehaviour
{
    public TextMeshProUGUI WinMessage;
    public GameObject WinCanvas;


    private void Awake()
    {
        WinCanvas.SetActive(false);
    }

    public void WinGame(string winner)
    {
        WinCanvas.SetActive(true);
        WinMessage.text = winner + " has won!";
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void ExitGame()
    {
        Debug.Log("Application exit if not in editor.");
        Application.Quit(0);
    }
}
