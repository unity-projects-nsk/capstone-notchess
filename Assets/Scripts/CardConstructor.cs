﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardConstructor : MonoBehaviour
{
    DeckManager dm;

    public Card cardInfo;

    private void Awake()
    {
        dm = GameObject.FindGameObjectWithTag("DeckManager").GetComponent<DeckManager>();
    }

    public void ShowCardInfo()
    {
        dm.cardImage.sprite = cardInfo.cardImage;
        dm.cardName.text = cardInfo.cardName;
        dm.cardType.text = cardInfo.pieceType.ToString();
        dm.cardAttack.text = cardInfo.attack.ToString();
        dm.cardDefense.text = cardInfo.defense.ToString();
        dm.cardSpeed.text = cardInfo.speed.ToString();
        dm.cardMaxHealth.text = cardInfo.maxHealth.ToString();
        dm.cardDescription.text = cardInfo.description;

        dm.currentlySelectedCard = cardInfo;
    }
}
