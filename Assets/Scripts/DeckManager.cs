﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.UI;

public enum PieceLocation { Pawn_1, Pawn_2, Pawn_3, Pawn_4, Pawn_5, Pawn_6, Pawn_7, Pawn_8, Rook_Left, Knight_Left, Bishop_Left, King, Queen, Bishop_Right, Knight_Right, Rook_Right }

public class DeckManager : SerializedMonoBehaviour
{
    public Image cardImage;
    public TextMeshProUGUI cardName, cardType, cardAttack, cardDefense, cardSpeed, cardMaxHealth, cardDescription;

    public List<GameObject> whiteDeck;
    public List<GameObject> blackDeck;

    public Card[] whiteDeckCards = new Card[16];
    public Card[] blackDeckCards = new Card[16];

    public List<Card> availableCards;

    public GameObject deckContent;
    public GameObject cardPrefab; //Placeholder?

    public Card currentlySelectedCard;

    public Button blackDeckButton;
    public Button whiteDeckButton;

    public GameObject blackDeckDisplay;
    public GameObject whiteDeckDisplay;

    private void Start()
    {
        BuildCards();
    }

    // Simply adds a button for each card available
    // Will later literally build cards using images.
    void BuildCards()
    {
        // Set content window to correct size based on number of available
        // cards. This makes the scroll bar auto-fit to the content.
        deckContent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, (Mathf.Ceil(availableCards.Count / 4)) * 160);

        // Assign relevant image to cards and set card constructor to hold
        // card info for display
        foreach (Card card in availableCards)
        {
            GameObject obj = Instantiate(cardPrefab, deckContent.transform);
            obj.GetComponent<CardConstructor>().cardInfo = card;
            obj.GetComponent<Button>().image.sprite = card.cardImage;
        }
    }

    public void OpenBlackDeck()
    {
        blackDeckButton.interactable = false;
        whiteDeckButton.interactable = true;
        blackDeckDisplay.SetActive(true);
        whiteDeckDisplay.SetActive(false);
    }

    public void OpenWhiteDeck()
    {
        whiteDeckButton.interactable = false;
        blackDeckButton.interactable = true;
        whiteDeckDisplay.SetActive(true);
        blackDeckDisplay.SetActive(false);
    }
}
