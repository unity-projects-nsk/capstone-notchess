﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

public class MoveSelectionHandler : SerializedMonoBehaviour
{
    // Make these material shaders later
    public GameObject movementLocationHighlight;
    public GameObject tileHighlight;
    public GameObject attackLocationHighlight;

    Camera cam;
    GameObject tileHighlightObject;
    GameObject movingPieceObject;
    List<Vector2Int> movementLocations;
    List<GameObject> movementLocationHighlights;

    void Start()
    {
        cam = Camera.main;

        enabled = false;
        tileHighlightObject = Instantiate(tileHighlight, Grid.GridPointToPoint(new Vector2Int(0, 0)), Quaternion.identity, transform);
        tileHighlightObject.SetActive(false);
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 point = hit.point;
            Vector2Int gridPoint = Grid.PointToGridPoint(point);

            tileHighlightObject.SetActive(true);
            tileHighlightObject.transform.position = Grid.GridPointToPoint(gridPoint);

            if (Input.GetMouseButtonDown(0))
            {
                // Check that move location is valid
                if (!movementLocations.Contains(gridPoint))
                    return;

                // Checks to make sure UI is not open
                if (EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.deckBuildingCanvas.activeSelf)
                    return;

                if (GameManager.Instance.PieceAtGridPoint(gridPoint) == null)
                {
                    GameManager.Instance.MovePiece(movingPieceObject, gridPoint);
                }
                else
                {
                    GameManager.Instance.tbm.StartBattle(movingPieceObject.GetComponent<Piece>(), GameManager.Instance.PieceAtGridPoint(gridPoint).GetComponent<Piece>(), movingPieceObject, gridPoint);
                    // GameManager.Instance.CaptureAt(gridPoint); // Eventually call turn based system here
                    // GameManager.Instance.MovePiece(movingPieceObject, gridPoint);
                }

                // Capture enemy piece
                ExitState();
                        
            }
        }
        else
        {
            tileHighlightObject.SetActive(false);
        }
    }

    private void CancelMovement()
    {
        enabled = false;

        foreach (GameObject movementLocationHighlight in movementLocationHighlights)
            Destroy(movementLocationHighlight);

        GameManager.Instance.DeselectPiece(movingPieceObject);
        TileSelectionHandler tsm = GetComponent<TileSelectionHandler>();
        tsm.EnterState();
    }

    public void EnterState(GameObject pieceToAlter)
    {
        movingPieceObject = pieceToAlter;
        enabled = true;

        movementLocations = GameManager.Instance.GetMovementForPiece(movingPieceObject);
        movementLocationHighlights = new List<GameObject>();

        if (movementLocations.Count == 0)
            CancelMovement();

        foreach(Vector2Int movementLocation in movementLocations)
        {
            GameObject highlight;

            if (GameManager.Instance.PieceAtGridPoint(movementLocation))
                highlight = Instantiate(attackLocationHighlight, Grid.GridPointToPoint(movementLocation), Quaternion.identity, transform);
            else
                highlight = Instantiate(movementLocationHighlight, Grid.GridPointToPoint(movementLocation), Quaternion.identity, transform);

            movementLocationHighlights.Add(highlight);
        }
    }

    private void ExitState()
    {
        enabled = false;
        TileSelectionHandler tsm = GetComponent<TileSelectionHandler>();
        tileHighlightObject.SetActive(false);
        GameManager.Instance.DeselectPiece(movingPieceObject);
        movingPieceObject = null;
        GameManager.Instance.ChangePlayers();
        tsm.EnterState();

        foreach (GameObject movementLocationHighlight in movementLocationHighlights)
            Destroy(movementLocationHighlight);
    }
}


