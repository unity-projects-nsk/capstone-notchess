﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

public class TileSelectionHandler : SerializedMonoBehaviour
{
    public GameObject tileHighlight;

    Camera cam;
    GameObject tileHighlightObject;

    void Start()
    {
        cam = Camera.main;

        Vector2Int gridPoint = Grid.ToGridPoint(0, 0);
        Vector3 point = Grid.GridPointToPoint(gridPoint);
        tileHighlightObject = Instantiate(tileHighlight, point, Quaternion.identity, transform);
        tileHighlightObject.SetActive(false);
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 point = hit.point;
            Vector2Int gridPoint = Grid.PointToGridPoint(point);

            tileHighlightObject.SetActive(true);
            tileHighlightObject.transform.position = Grid.GridPointToPoint(gridPoint);

            if (Input.GetMouseButtonDown(0))
            {
                // Checks to make sure UI is not open
                if (EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.deckBuildingCanvas.activeSelf)
                    return;

                GameObject pieceSelection = GameManager.Instance.PieceAtGridPoint(gridPoint);

                if (GameManager.Instance.IsPieceOnCurrentTeam(pieceSelection))
                {
                    GameManager.Instance.SelectPiece(pieceSelection);
                    ExitState(pieceSelection);
                }
            }
        }
        else
        {
            tileHighlightObject.SetActive(false);
        }
    }

    public void EnterState()
    {
        enabled = true;
    }

    private void ExitState(GameObject pieceMoving)
    {
        enabled = false;
        tileHighlightObject.SetActive(false);
        MoveSelectionHandler movement = GetComponent<MoveSelectionHandler>();
        movement.EnterState(pieceMoving);
    }
}


