﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

/*
    * This class defines the playing Grid. It houses static helper classes
    * that convert worldspace coordinate points to gridpoints and vice versa.
    */
public class Grid
{
    // Converts a given gridpoint in Vector2Int format to worldspace coordinates.
    static public Vector3 GridPointToPoint(Vector2Int gridPoint)
    {
        float xPos = -3.5f + 1.0f * gridPoint.x;
        float zPos = -3.5f + 1.0f * gridPoint.y;
        return new Vector3(xPos, 0, zPos);
    }

    // Converts a given worldspace coordinate to a gridpoint in Vector2Int format.
    static public Vector2Int PointToGridPoint(Vector3 point)
    {
        int column = Mathf.FloorToInt(4.0f + point.x);
        int row = Mathf.FloorToInt(4.0f + point.z);
        return new Vector2Int(column, row);
    }

    // Creates a new Vector2Int within a contextualized method.
    static public Vector2Int ToGridPoint(int column, int row)
    {
        return new Vector2Int(column, row);
    }
}