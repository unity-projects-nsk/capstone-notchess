﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BoardManager : SerializedMonoBehaviour
{
    // Define materials for base and selected to show piece selection
    public Material originalBlackMaterial;
    public Material originalWhiteMaterial;
    public Material selectionMaterial;

    // Add the provided gameobject to the board at gridpoint (column, row) and
    // return the instantiated gameobject.
    public GameObject AddPiece(GameObject pieceToAdd, int column, int row)
    {
        Vector2Int gridPoint = Grid.ToGridPoint(column, row);
        GameObject addedPiece = Instantiate(pieceToAdd, Grid.GridPointToPoint(gridPoint), Quaternion.identity, transform);
        return addedPiece;
    }

    // Destroy provided gameobject/piece.
    public void RemovePiece(GameObject pieceToRemove)
    {
        Destroy(pieceToRemove);
    }

    // Move given gameobject to new position based on gridpoint in Vector2Int format.
    public void MovePiece(GameObject pieceToMove, Vector2Int gridPoint)
    {
        //Debug.Log("BM.MovePiece: " + pieceToMove.name + " at " + gridPoint.ToString());
        pieceToMove.transform.position = Grid.GridPointToPoint(gridPoint);
    }

    // Select given gameobject and telegraph selection by changing material.
    public void SelectPiece(GameObject pieceToSelect)
    {
        MeshRenderer mr = pieceToSelect.GetComponentInChildren<MeshRenderer>();
        mr.material = selectionMaterial;
    }

    // Deselect given gameobject and telegraph deselection by reverting material to original.
    public void DeselectPiece(GameObject pieceToDeselect)
    {
        MeshRenderer mr = pieceToDeselect.GetComponentInChildren<MeshRenderer>();
        if (GameManager.Instance.currentPlayer.isWhite)
            mr.material = originalWhiteMaterial;
        else
            mr.material = originalBlackMaterial;
    }
}


