﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Card", menuName = "Card")]
public class Card : SerializedScriptableObject
{
    public string cardName;
    public PieceType pieceType;
    public GameObject piecePrefab;
    public Sprite cardImage;
    public int attack, defense, speed, maxHealth;
    [TextArea]
    public string description;
}
