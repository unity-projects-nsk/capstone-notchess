﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TBManager : MonoBehaviour
{
    public GameObject TBCanvas;
    public TextMeshProUGUI attackerNameLabel, defenderNameLabel;
    public TextMeshProUGUI attackerHealthLabel, defenderHealthLabel;
    public TextMeshProUGUI log;

    public Button AttackerAttackAction;
    public Button AttackerDefendAction;
    public Button DefenderAttackAction;
    public Button DefenderDefendAction;

    int attackerMaxHealth, defenderMaxHealth;
    int attackerHealth, defenderHealth;
    int attackerDamage, defenderDamage;
    bool attackerDefending, defenderDefending;
    Piece attacker, defender;

    GameObject attackerPiece;
    Vector2Int defenderLocation;

    // Start is called before the first frame update
    void Awake()
    {
        TBCanvas.SetActive(false);
    }

    public void StartBattle(Piece attacker, Piece defender, GameObject movingPieceObject, Vector2Int gridPoint)
    {
        defenderLocation = gridPoint;
        attackerPiece = movingPieceObject;

        InitializeBattleInfo(attacker, defender);

        TBCanvas.SetActive(true);
    }

    void InitializeBattleInfo(Piece attacker, Piece defender)
    {
        // Set Variables
        attackerMaxHealth = attacker.cardInfo.maxHealth;
        defenderMaxHealth = defender.cardInfo.maxHealth;
        attackerHealth = attackerMaxHealth;
        defenderHealth = defenderMaxHealth;
        attackerDamage = attacker.cardInfo.attack;
        defenderDamage = defender.cardInfo.attack;
        attackerDefending = false;
        defenderDefending = false;
        this.attacker = attacker;
        this.defender = defender;

        // Set Text Labels
        attackerNameLabel.text = "Attacker: " + attacker.cardInfo.cardName;
        defenderNameLabel.text = "Defender: " + defender.cardInfo.cardName;
        attackerHealthLabel.text = attackerHealth + "/" + attackerMaxHealth;
        defenderHealthLabel.text = defenderHealth + "/" + defenderMaxHealth;
    }

    // Attack other player
    public void Attack(int playerIndex) // 0 for attacker, 1 for defender
    {
        if (playerIndex == 0) // If attacker
        {
            if (defenderDefending)
            {
                defenderHealth -= (attackerDamage / 2);
                defenderDefending = false;
                defenderHealthLabel.text = defenderHealth + "/" + defenderMaxHealth;

                log.text = attacker.cardInfo.cardName + " hit " + defender.cardInfo.cardName + " for " + (attackerDamage / 2) + " damage!"; 

                if (defenderHealth <= 0)
                    EndMatch(0);
            }
            else
            {
                defenderHealth -= attackerDamage;
                defenderHealthLabel.text = defenderHealth + "/" + defenderMaxHealth;

                log.text = attacker.cardInfo.cardName + " hit " + defender.cardInfo.cardName + " for " + attackerDamage + " damage!";

                if (defenderHealth <= 0)
                    EndMatch(0);
            }
        }

        if (playerIndex == 1) // If defender
        {
            if (attackerDefending)
            {
                attackerHealth -= (defenderDamage / 2);
                attackerDefending = false;
                attackerHealthLabel.text = attackerHealth + "/" + attackerMaxHealth;

                log.text = defender.cardInfo.cardName + " hit " + attacker.cardInfo.cardName + " for " + (defenderDamage / 2) + " damage!";

                if (attackerHealth <= 0)
                    EndMatch(1);
            }
            else
            {
                attackerHealth -= defenderDamage;
                attackerHealthLabel.text = attackerHealth + "/" + attackerMaxHealth;

                log.text = defender.cardInfo.cardName + " hit " + attacker.cardInfo.cardName + " for " + defenderDamage + " damage!";

                if (attackerHealth <= 0)
                    EndMatch(1);
            }
        }

        SwitchPlayerTurns();
    }

    // Defend Self (halves incoming damage)
    public void Defend(int playerIndex) // 0 for attacker, 1 for defender
    {
        if (playerIndex == 0)
            attackerDefending = true;
        if (playerIndex == 1)
            defenderDefending = true;

        SwitchPlayerTurns();
    }

    void EndMatch(int playerIndex) // 0 for attacker, 1 for defender
    {
        if (playerIndex == 0)
        {
            GameManager.Instance.winnerLabel = GameManager.Instance.nonCurrentPlayer.playerName;
            GameManager.Instance.CaptureAt(defenderLocation);
            GameManager.Instance.MovePiece(attackerPiece, defenderLocation);
        }

        TBCanvas.SetActive(false);
    }

    void SwitchPlayerTurns()
    {
        AttackerAttackAction.interactable = !AttackerAttackAction.interactable;
        AttackerDefendAction.interactable = !AttackerDefendAction.interactable;
        DefenderAttackAction.interactable = !DefenderAttackAction.interactable;
        DefenderDefendAction.interactable = !DefenderDefendAction.interactable;
    }
}
